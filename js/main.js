$(document).ready(function () {
    $("#btn").click(() => {
        $("#btn").hide();
        $("body").append("<input type='text' placeholder='Введите диаметр круга?'>");
        $("body").append("<input type='text' placeholder='Введите цвет?'>");
        $("body").append("<button class='button' id='create'>Нарисовать</button>");
        $("#create").click(() => {
            let diam = +$("input:first").val();
            let color = $("input:last").val();
            $("#create,input").hide();
            $("body").append(`<div class="circle" style=width:${diam}px;height:${diam}px;border-radius:50%;display:inline-block;background-color:${color};margin:10px></div>`);
        });
    });
});


